package main

import (
	"bufio"
	"crypto/rand"
	"crypto/rsa"
	"encoding/hex"
	"fmt"
	"net"
	"os"
	"strconv"

	"./multi"
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		fmt.Println("Invalid Syntax.\nUsage: go run server.go <Listening Address:Port> <Path to store data at>")
		os.Exit(69)
	}
	startServer(args[0], args[1])
}

func startServer(addr string, path string) {
	fmt.Println("Starting Listener")
	privateKey, _ := rsa.GenerateKey(rand.Reader, 2048)
	publicKey := privateKey.PublicKey
	listner, _ := net.Listen("tcp", addr)
	for {
		name := make([]byte, 16)
		rand.Read(name)
		conn, _ := listner.Accept()
		fmt.Println("Connection Established from: " + conn.RemoteAddr().String() + "\nSending encryption key.")
		fmt.Fprintf(conn, publicKey.N.String()+"\n")
		bufio.NewReader(conn).ReadString('`')
		fmt.Fprintf(conn, strconv.FormatInt(int64(publicKey.E), 10)+"\n")
		client := multi.New(hex.EncodeToString(name), conn.RemoteAddr(), conn, privateKey)
		go multi.ListenFromClient(client, path)
	}

}
