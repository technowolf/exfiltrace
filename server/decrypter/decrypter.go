package decrypter

import (
	"crypto/cipher"
)

//Decrypter Struct to handle passkey and cipher
type Decrypter struct {
	passkey      []byte
	cipher       cipher.Block
	iv           []byte
	CFBDecrypter cipher.Stream
}

//New Constructor for decrypter
func New(passkey []byte, iv []byte, c cipher.Block) Decrypter {
	CFBDecrypter := cipher.NewCFBDecrypter(c, iv)
	d := Decrypter{passkey, c, iv, CFBDecrypter}
	return d
}

//DecryptToOriginal decrypts the recieved data and store it to output file
func DecryptToOriginal(d Decrypter, data []byte) []byte {
	decryptedData := make([]byte, len(data))
	d.CFBDecrypter.XORKeyStream(decryptedData, data)
	return decryptedData
}
