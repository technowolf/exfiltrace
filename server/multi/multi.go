package multi

import (
	"bufio"
	"crypto/aes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net"
	"os"
	"strings"

	"../decrypter"
)

//Client struct for using with multi handler
type Client struct {
	ClientName string
	ClientIP   net.Addr
	Connection net.Conn
	PrivateKey *rsa.PrivateKey
}

//New constructor for Client struct
func New(ClientName string, ClientIP net.Addr, Connection net.Conn, privateKey *rsa.PrivateKey) Client {
	c := Client{ClientName, ClientIP, Connection, privateKey}
	return c
}

// ListenFromClient over connection c.conn
func ListenFromClient(c Client, p string) {

	fmt.Println("Client: ", c.ClientName)
	path := p + "/" + c.ClientName
	os.MkdirAll(path, os.ModePerm)
	reader := bufio.NewReader(c.Connection)
	data, _ := reader.ReadString('\n')
	fmt.Println("Recieved AES KEY and IVs successfully!")
	data = strings.TrimSuffix(data, "\n")
	EncryptedData, _ := hex.DecodeString(data)
	passkey, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, c.PrivateKey, EncryptedData, nil)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(2)
	}
	cipher, _ := aes.NewCipher(passkey)
	data, _ = reader.ReadString('\n')
	data = strings.TrimSuffix(data, "\n")
	EncryptedData, _ = hex.DecodeString(data)
	iv, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, c.PrivateKey, EncryptedData, nil)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(2)
	}
	d := decrypter.New(passkey, iv, cipher)
	for {
		EncryptedData, err = receive(reader, c.Connection)
		if err != nil {
			break
		}
		fileName := decrypter.DecryptToOriginal(d, EncryptedData)
		fmt.Println("Receiving File: " + string(fileName))
		if _, err := os.Stat(path + "/" + string(fileName)[0:strings.LastIndex(string(fileName), "/")]); os.IsNotExist(err) {
			os.MkdirAll(path+"/"+string(fileName)[0:strings.LastIndex(string(fileName), "/")], 0766)
		}
		targetFile, _ := os.Create(path + "/" + string(fileName))
		EncryptedData, err = receive(reader, c.Connection)
		if err != nil {
			break
		}
		DecryptedData := decrypter.DecryptToOriginal(d, EncryptedData)
		targetFile.Write(DecryptedData)
		targetFile.Close()
		fmt.Println("File Received!")

	}
}

func receive(reader *bufio.Reader, conn net.Conn) ([]byte, error) {
	data, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Connection Terminated!")
		conn.Close()
		return nil, err
	}
	data = strings.TrimSuffix(data, "\n")
	EncryptedData, _ := hex.DecodeString(data)
	return EncryptedData, nil
}
